FROM node:latest

RUN apt-get update -y && apt-get install -y vim
COPY . /opt/arbitrage
WORKDIR /opt/arbitrage
RUN npm install
CMD npm start

